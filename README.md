# GSPA Studentbudget Calculator Module

A customized student budget calculator configured for GSPA.

Default initial values can be configured from the GSPA Budget calculator settings page found in the Dashboard menu.

## Programs and Tuition

Program and tuition values can be entered in the settings page.

Each program should be on its own line and in the following format:

```Program Name|Domestic Tuition|International Tuition```

Please enter tuition without commas.

Example:
```
Chemical Engineering - MEng|2760.00|11508.00
Chemistry - PhD (Nanotechnology)|2254.00|6972.00
Electrical and Computer Engineering - MEng (Electric Power Engineering)|3452.00|3950.00
```

## Residence

Residence listing works similar to the program and tuition section

Enter each residence entry on its own line.

Each residence entry will have the following format:

```Residence Name|Price```

Example:
```
Single room – UW Place (Two-bedroom suite)|7069.00
Single room – Mackenzie King Village (Four-bedroom suite)|7823.00
Double room – St. Paul's University College|11598.00
Single room – St. Paul's University College|12467.00
Double room – St. Jerome's University|11752.00
Single room – St. Jerome's University|12552.00
Double room – Renison University College|11070.00
Double room – Conrad Grebel University College|11320.00
Interconnecting (semi-private) room – Village 1 or Ron Eydt Village|6255.00
Double room – Village 1 or Ron Eydt Village|5879.00
Single room – Village 1 or Ron Eydt Village|6553.00
Double room (semi-private bathroom) – Renison University College|11390.00
Single room – UW Place (Three- or four-bedroom suite)|6691.00
Double room – UW Place|6174.00
Single room – Columbia Lake Village-South (Four-bedroom townhouse)|6250.00
Single room – Claudette Millar Hall|7188.00
Double room (semi-private) – Claudette Millar Hall|6851.00
```



