/*
 * @file uw_studentbudget_calculator_gspa.js
 * Main library for the uw_studentbudget_calculator  module
 */

(function ($) {
	Drupal.behaviors.exampleModule = {
		attach: function (context, settings) {
			switch($('input:radio[name=uw_studentbudget_calculator_res_boolean]:checked').val()) {
				case "0":
					$("#uw_studentbudget_calculator_residence").show();
					$("#uw_studentbudget_calculator_offcampus").hide();
					uw_studentbudget_calculator.calculate();
					break;
				default:
					$("#uw_studentbudget_calculator_residence").hide();
					$("#uw_studentbudget_calculator_offcampus").show();
					uw_studentbudget_calculator.calculate();
					break;
			}

			uw_studentbudget_calculator.calculate();

			// On program change or domestic/international change, update tuition
			$("#edit-uw-studentbudget-calculator-program, input:radio[name=location]").change( function() {
				var location = $("input:radio[name=location]:checked").val();
				var program = $("#edit-uw-studentbudget-calculator-program").val();
				var url = window.location.href.split('?')[0];
				$.ajax({
					dataType: "json",
					url: url + "/query/program/" + location + "/" + program,
					success: function(result) {
						$("#edit-uw-studentbudget-calculator-tuition").val(result.tuition);
						uw_studentbudget_calculator.calculate();
					}
				});
			});

			// On residence change, update cost and meal plan options
			$("#edit-uw-studentbudget-calculator-res-name").change( function() {
				$("#edit-uw-studentbudget-calculator-res-value").val($("#edit-uw-studentbudget-calculator-res-name").val());
				uw_studentbudget_calculator.calculate();
			});

			$("input:radio[name=uw_studentbudget_calculator_res_boolean]").click(function() {
				switch($(this).val()) {
					case "0":
						$("#uw_studentbudget_calculator_residence").show();
						$("#uw_studentbudget_calculator_offcampus").hide();
						uw_studentbudget_calculator.calculate();
						break;
					default:
						$("#uw_studentbudget_calculator_residence").hide();
						$("#uw_studentbudget_calculator_offcampus").show();
						uw_studentbudget_calculator.calculate();
						break;
				}
			});

			$(".uw_studentbudget_calculator_monthly").change(function() {
			  if ($(this).val() == '') {
          $("#uw_studentbudget_calculator_" + $(this).attr("pair") + "_total").text("$0.00");
			  }
			  else {
          $("#uw_studentbudget_calculator_" + $(this).attr("pair") + "_total").text("$" + (parseFloat(($(this).val()).replace(/,/g,'')) * 12).toFixed(2));
        }
			});

			$("form").bind("reset", function(e) {
				window.location = "interactive-budget";
			});

			uw_studentbudget_calculator.calculate();
      $(".uw_studentbudget_calculator_monthly").trigger('change');
		}
	};
})(jQuery);
/**
 * Empty static class wrapper
 */
function uw_studentbudget_calculator() {};

/**
 * Returns an integer representation of the value read from a input field
 * Returns 0 if no valid value can be read
 */
uw_studentbudget_calculator.getFloatValue = function(id) {
	  try {
	    var element = document.getElementById(id);
	    var value = parseFloat(element.value.replace(/,/g,''));
	    if(isNaN(value)) {
	      //element.value = 0;
	      return 0;
	    }
	    else {
	      var returnItem = value.toFixed(2);
	      if(returnItem.length < 9) {
	        element.value = returnItem;
	        return value;
	      } else {
	    	element.value = 0;
	        return 0.00;
	      }
	    }
	  }
	  catch(e) {
	    return 0;
	  }
	}

/**
 * Calculates the two values (amount * month) and sets the totalId holder with that information
 *
 * @return      Calculated value
 */
uw_studentbudget_calculator.setCalculatedValue = function(totalId, amountId, monthId) {
  var calculatedValue = uw_studentbudget_calculator.getFloatValue(amountId) * uw_studentbudget_calculator.getFloatValue(monthId);
  document.getElementById(totalId).innerHTML = '$' + calculatedValue.toFixed(2);
  return calculatedValue;
}

/**
 * Calculate the value from the forms on the fly
 */
uw_studentbudget_calculator.calculate = function() {
  // Resources total
  var totalResources = (uw_studentbudget_calculator.getFloatValue("edit-uw-studentbudget-calculator-beginbalance")
    + uw_studentbudget_calculator.getFloatValue("edit-uw-studentbudget-calculator-awards")
    + uw_studentbudget_calculator.getFloatValue("edit-uw-studentbudget-calculator-osap-canada")
  );
  document.getElementById("uw_studentbudget_calculator_monthlyresources_calculated_value").innerHTML = "$" + totalResources.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

  // Expenses total
  var totalExpenses = (uw_studentbudget_calculator.getFloatValue("edit-uw-studentbudget-calculator-tuition")
    + uw_studentbudget_calculator.getFloatValue("edit-uw-studentbudget-calculator-books")
    + uw_studentbudget_calculator.getFloatValue("edit-uw-studentbudget-calculator-groceries") * 12
    + uw_studentbudget_calculator.getFloatValue("edit-uw-studentbudget-calculator-personal") * 12
  );

  if (document.getElementById('edit-uw-studentbudget-calculator-res-boolean-0').checked) {
    var livingOnCampusExpenses = uw_studentbudget_calculator.getFloatValue("edit-uw-studentbudget-calculator-res-value");
    totalExpenses += livingOnCampusExpenses;
  }
  else {
    var livingOffCampusExpenses = uw_studentbudget_calculator.getFloatValue("edit-uw-studentbudget-calculator-offcampusrent");
    totalExpenses += livingOffCampusExpenses * 12;
  }
  document.getElementById("uw_studentbudget_calculator_monthlyexpenses_calculated_value").innerHTML = "$" + totalExpenses.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  // Financial need
  document.getElementById("uw_studentbudget_calculator_netresources_calculated_value").innerHTML = "$" + (totalResources - totalExpenses).toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}
